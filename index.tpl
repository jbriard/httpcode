<!doctype html>
<!-- index.tpl -->
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="description" content="HTTP Code, simply test http response"/>
        <meta name="robots" content="index,follow"/>
        <title>HTTP Code</title>
</head>
<body>

<h2>HTTP Code</h2>
<p>Is just a simple way to test http response.</p>

<p>Add the code you want to use at the end of the URL <i>http://httpcode.otbm.fr/[code]</i><br />
For exemple generating a 403 page : <a href="https://httpcode.otbm.fr/403">https://httpcode.otbm.fr/403</a></p>


<pre>curl -I https://httpcode.otbm.fr/403
HTTP/1.0 403 Forbidden
Server: otbm.fr
Date: Wed, 06 May 2020 18:47:25 GMT
Content-Length: 24
Content-Type: text/html; charset=UTF-8</pre>



	<h3>All the response</h3>
	<p>
		Code <a href=100>100</a> : Continue<br />
Code <a href=101>101</a> : Switching Protocols<br />
Code <a href=102>102</a> : Processing<br />
Code <a href=200>200</a> : OK<br />
Code <a href=201>201</a> : Created<br />
Code <a href=202>202</a> : Accepted<br />
Code <a href=203>203</a> : Non-authoritative Information<br />
Code <a href=204>204</a> : No Content<br />
Code <a href=205>205</a> : Reset Content<br />
Code <a href=206>206</a> : Partial Content<br />
Code <a href=207>207</a> : Multi-Status<br />
Code <a href=208>208</a> : Already Reported<br />
Code <a href=226>226</a> : IM Used<br />
Code <a href=300>300</a> : Multiple Choices<br />
Code <a href=301>301</a> : Moved Permanently<br />
Code <a href=302>302</a> : Found<br />
Code <a href=303>303</a> : See Other<br />
Code <a href=304>304</a> : Not Modified<br />
Code <a href=305>305</a> : Use Proxy<br />
Code <a href=307>307</a> : Temporary Redirect<br />
Code <a href=308>308</a> : Permanent Redirect<br />
Code <a href=400>400</a> : Bad Request<br />
Code <a href=401>401</a> : Unauthorized<br />
Code <a href=402>402</a> : Payment Required<br />
Code <a href=403>403</a> : Forbidden<br />
Code <a href=404>404</a> : Not Found<br />
Code <a href=405>405</a> : Method Not Allowed<br />
Code <a href=406>406</a> : Not Acceptable<br />
Code <a href=407>407</a> : Proxy Authentication Required<br />
Code <a href=408>408</a> : Request Timeout<br />
Code <a href=409>409</a> : Conflict<br />
Code <a href=410>410</a> : Gone<br />
Code <a href=411>411</a> : Length Required<br />
Code <a href=412>412</a> : Precondition Failed<br />
Code <a href=413>413</a> : Payload Too Large<br />
Code <a href=414>414</a> : Request-URI Too Long<br />
Code <a href=415>415</a> : Unsupported Media Type<br />
Code <a href=416>416</a> : Requested Range Not Satisfiable<br />
Code <a href=417>417</a> : Expectation Failed<br />
Code <a href=418>418</a> : I'm a teapot<br />
Code <a href=424>424</a> : Misdirected Request<br />
Code <a href=422>422</a> : Unprocessable Entity<br />
Code <a href=423>423</a> : Locked<br />
Code <a href=424>424</a> : Failed Dependency<br />
Code <a href=426>426</a> : Upgrade Required<br />
Code <a href=428>428</a> : Precondition Required<br />
Code <a href=429>429</a> : Too Many Requests<br />
Code <a href=431>431</a> : Request Header Fields Too Large<br />
Code <a href=444>444</a> : Connection Closed Without Response<br />
Code <a href=451>451</a> : Unavailable For Legal Reasons<br />
Code <a href=499>499</a> : Client Closed Request<br />
Code <a href=500>500</a> : Internal Server Error<br />
Code <a href=501>501</a> : Not Implemented<br />
Code <a href=502>502</a> : Bad Gateway<br />
Code <a href=503>503</a> : Service Unavailable<br />
Code <a href=504>504</a> : Gateway Timeout<br />
Code <a href=505>505</a> : HTTP Version Not Supported<br />
Code <a href=506>506</a> : Variant Also Negotiates<br />
Code <a href=507>507</a> : Insufficient Storage<br />
Code <a href=508>508</a> : Loop Detected<br />
Code <a href=510>510</a> : Not Extended<br />
Code <a href=511>511</a> : Network Authentication Required<br />
Code <a href=599>599</a> : Network Connect Timeout Error<br />
Code <a href=520>520</a> : Unknown Error<br />
Code <a href=521>521</a> : Web Server Is Down<br />
Code <a href=522>522</a> : Connection Timed Out<br />
Code <a href=523>523</a> : Origin Is Unreachable<br />
Code <a href=524>524</a> : A Timeout Occurred<br />
Code <a href=525>525</a> : SSL Handshake Failed<br />
Code <a href=256>256</a> : Invalid SSL Certificate<br />
Code <a href=527>527</a> : Railgun Error<br />
Code <a href=495>495</a> : SSL Certificate Error<br />
Code <a href=496>496</a> : SSL Certificate Required<br />
Code <a href=497>497</a> : HTTP Request Sent to HTTPS Port<br />
Code <a href=498>498</a> : Token expired/invalid<br />
Code <a href=499>499</a> : Client Closed Request<br />

	</p>

</body>
</html>
