FROM python:slim

WORKDIR /usr/src/httpcode

COPY . . 

RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8444

CMD [ "python", "./http_code.py" ]
