#!/usr/bin/python3

from bottle import (
    route,
    run,
    request,
    response,
    HTTPError,
    error,
    install,
    view,
    HTTPResponse,
)
import json
import urllib3, logging
import requests
import sys


class out:

    INFO, WARN, ERROR = ((32, "INFO"), (33, "WARN"), (31, "ERROR"))

    @staticmethod
    def _output(message, type):
        sys.stdout.write("[\033[1;%im%s\033[0;0m] " % type + "%s\n" % message)

    @staticmethod
    def info(message):
        out._output(message, out.INFO)

    @staticmethod
    def warn(message):
        out._output(message, out.WARN)

    @staticmethod
    def error(message):
        out._output(message, out.ERROR)
        sys.exit(1)


@route("/", method="GET")
@view("index.tpl")
def index():
    url = request.headers["HOST"]
    try:
        response.status = 200

    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@route("/<code>", method="GET")
def getCode(code):
    """

    """
    try:
        ret = getCodeInfos(code)
        response.status = int(code)
        return "<pre>%s %s</pre>" % (code, ret)
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@route("/checkhealth", method="GET")
def get_checkhealth():
    try:
        response.status = 200
        return {"status": "ok"}
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@route("/version", method="GET")
def get_version():
    try:
        response.status = 200
        return version
    except Exception as e:
        response.status = 500
        return {"status": "error", "error": str(e)}


@error(404)
def error404(error):
    return {"status": "error", "error": "not found"}


def getCodeInfos(code):
    try:
        ret = ""

        for i in code_json_data["httpcode"]:
            if i["code"] == code:
                ret = i["description"]
            if not ret:
                ret = "Unknown code"

    except Exception as e:
        raise Exception("Something Bad append - %s" % e)
    return ret


def main():
    out.info("Starting HTTP Codes")
    out.info("Reading httpcode.conf")
    try:
        with open("httpcode.conf") as f:
            c = json.load(f)
    except IOError:
        out.error("Could not read httpcode.conf file (missing? permission?)")
    except ValueError:
        out.error("Could not parse httpcode.conf file (not valid json?)")

    try:
        global port
        port = c["http"]["port"]
    except Exception as e:
        out.error("Could not find %s on httpcode.conf (missing? out of scope ?)" % e)

    try:
        host = c["http"]["host"]
    except Exception as e:
        out.error("Could not find %s on httpcode.conf (missing? out of scope ?)" % e)

    try:
        global version
        version = c["server"]["version"]
    except Exception as e:
        out.error("Could not find %s on httpcode.conf (missing? out of scope ?)" % e)
    out.info("Done")
    ## Read json code file
    out.info("Reading httpcode.json")
    try:
        global code_json_data
        with open("httpcode.json") as json_file:
            code_json_data = json.load(json_file)
    except IOError:
        out.error("Could not read httpcode.json file (missing? permission?)")
    except ValueError:
        out.error("Could not parse httpcode.json file (not valid json?)")
    out.info("Done")

    out.info("Configuration files loaded")

    out.info("Starting http server")

    logging.getLogger("wsgi").addHandler(logging.NullHandler())
    run(
        host=host,
        port=port,
        quiet=True,
        server="paste",
        use_threadpool=True,
        threadpool_workers=15,
        request_queue_size=5,
        server_version="otbm.fr",
    )


if __name__ == "__main__":
    main()
