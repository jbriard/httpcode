#!/usr/bin/python3


import json

def getCodeInfos():
	with open("../httpcode.json") as json_file:
	     data = json.load(json_file)

	for i in data["httpcode"]:
		 code = (i['code'])
	     ret = "<a href=%s>%s</a>" % (code, code)
	     return ret


liste=getCodeInfos()
